import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import config from './config.json';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import { Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';    

// Librerías: Stitch, speak-tts, etc.
import { Stitch, RemoteMongoClient, AnonymousCredential } from "mongodb-stitch-browser-sdk";
//import Speech from 'speak-tts'; // text to speach
import Otros from './componentes/Otros';
import Aesencial from './componentes/Aesencial';
import Login from './componentes/Login';
import Home from './componentes/Home';
import Avegetal from './componentes/Avegetal';
import CallMongo from './componentes/CallMongo';
import { ShowUnProducto } from './componentes/ShowUnProducto';
import { SelectBuscar} from './componentes/Template';
import { ImgHome} from './componentes/Template';
import ShowList from './componentes/ShowList';



// exportamos stitch para los Components que necesitan stitch.auth.user.id (Blog, Aceites, Alumnos, etc)
export const stitch = Stitch.initializeDefaultAppClient(config.appId);

// y mongo para utilizar comandos de mongo sin pasar por funciones de Stitch como:
// mongo.db('mydb').collection('mycollection').insertOne({})
export const mongo = stitch.getServiceClient(RemoteMongoClient.factory,'mongodb-atlas')

/* esto es para comprobar si ya se han logueado por gmail.
    if (stitch.auth.hasRedirectResult()) {
    stitch.auth.handleRedirectResult().then( u => { console.log(u);  window.location.href = '/' })
  } */
  
// si no esta logueado (de forma anonima ) que se logueé directemente
if ( !stitch.auth.isLoggedIn ) {
    stitch.auth.loginWithCredential( new AnonymousCredential() ) 
    .then( a => {console.log(a)})
    .catch()
}

export function nbsp(n) {
  let ret = []
  for (let index = 1; index <= n; index++) {
    ret.push(<span key={index}>&nbsp;</span>);    
  }
  return ret
}


export function br(n) {
  let ret = []
  for (let index = 1; index <= n; index++) {
    ret.push(<br key={index} />);    
  }
  return ret
}

/*  const imgHome = window.location.href.split('/')[3]=== 'home' || '' ? <ImgHome/>  : <span/>
 */
const navSup= (
  <div className="container-fliud footer">
    <div className="row">
      <div className="col-md-2 col-xs-12" id="logo">
          MUYARANS
      </div>
      
      <div className="hidden-sm-down col-md-8">
        <ul className="nav justifay-content-center">
          <li className="nav-item">
          <span class="material-icons">
          favorite  
          </span>
          <span>Lista de deseos</span>            
          </li>
          <li className="nav-item">
          <span class="material-icons">
          done 
          </span>
          <span>Tramitar Pedido</span>              
          </li>
          <li className="nav-item">
          <span class="material-icons">
          lock 
          </span>
          <span>Iniciar sesion</span>              
          </li>
        </ul>

      </div>
      <div className="col-md-2 hidden-sm-down" id="logo">
        <span class="material-icons">
          person
        </span>        
         Mi cuenta 
      </div>

    </div>

  </div>
) 
 
const barra = (
  <header className="container-fluid " >
    <div className="row">
    <div className="col-md-3 col-sx-1">
     <Link to="/" id="encabezado"> MUYARANS </Link>  
    </div>  
    <nav className="col-md-6" >
      <ul className="nav justifay-content-center">
        <li className="nav-item"> <Link  to = "/"  className="tituloF nav-link" > Inicio </Link></li>
        <li className="nav-item"><Link to="/ae" className="tituloF nav-link"> Aceites esenciales</Link> </li>
        <li className="nav-item"><Link to="/av" className="tituloF nav-link"> Aceites vegetales</Link> </li>
        <li className="nav-item"><Link to="/otros" className="tituloF nav-link"> Otros </Link> </li>
      </ul>
    </nav>
    <div id="selectBuscar" className="col-md-3">
      <SelectBuscar /> 
    </div>
    </div>
  </header> 
 
)

 const footer =(
  <footer className="footer text-center" >
    Copyright  2012-2013
  </footer>

) 

const routing = (
 
    <Router>
      {navSup}
      {barra}
      {/* {imgHome} */}
      <Switch>
        <Route exact path="/" component={Home} />    
        <Route path="/home" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/av" component={Avegetal} />
        <Route path="/ae" component={Aesencial} />
        <Route path="/show/:id" component={ShowUnProducto} />
        <Route path="/showL" component={ShowList} />
        <Route path="/otros" component={Otros} />
        <Route path="/aceites" component={CallMongo} />      
      </Switch> 
      {footer}
    </Router>
  
)

// me da fayo por que todavía no he creado la clase de rutas
ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
