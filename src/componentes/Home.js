import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ImgHome} from './Template';




export default class Home extends Component {
    render() {
        return (
            <div>
            <ImgHome/>
            <section className = "container">                
                <h1 className = "text-center"> COSMÉTICA Y MEDICINA NATURAL </h1>

                <p>La naturaleza te ofrece todo lo que necesitas para tu bienestar y desde esta pagina solo pretendo que tengas una opción mas a tu alcance, ya sea como un complemento a tu salud ( sin competir con la medicina tradicional por favor consulta a tu medico) o para poder hacer tus propios tratamientos caseros, sin conservates, paravenos etc. 
                </p>
                
                <div className="row">
                <article className="col-md-4">                    
                    <h2 className="text-center"> 
                        <Link to={'/ae'} className = "tituloF"> 
                            Acetes Esenciales 
                        </Link>  
                    </h2> 
                    <Link to={'/ae'}><img src={"https://firebasestorage.googleapis.com/v0/b/muyarantccia.appspot.com/o/homeImg%2Falternative-aroma-aromatherapy-aromatic-275755.jpg?alt=media&token=19bdf2c3-9f78-4fa3-a70e-4ec71e68f72c"} alt="aceites esenciales" className="img-thumbnail"/>
                    </Link> 
                    <p className = "realzar">
                        El uso de los aceites esenciales se remonta miles de años atrás, por lo que están considerados una de las formas más antiguas de cosmética y medicina.
                    </p> 
                </article>

                <article className="col-md-4">
                            <h2 className="text-center">
                                <Link to = {'/av'} className = "tituloF"> Acetes vegetales </Link>
                            </h2>
                            <Link to = {'/av'}>
                                <img src={"https://firebasestorage.googleapis.com/v0/b/muyarantccia.appspot.com/o/homeImg%2Ftwo-clear-glass-bottles-with-liquids-672051%20(1).jpg?alt=media&token=59ae0bed-be26-4d13-9efc-5c2246d8bcce"} alt="aceites vegetales" className="img-thumbnail"/>
                            </Link> 
                        <p className = "realzar">
                            Los aceites vegetales en cosmética han sido utilizados a lo largo de la historia de la humanidad.
                        </p>
                </article>

                <article className="col-md-4 ">
                                       
                    <h2 className="text-center"> 
                        <Link to = {'/otros'} className = "tituloF">
                            Otros productos
                        </Link>
                    </h2> 
                    <Link to = {'/otros'}> 
                    <img src={"https://firebasestorage.googleapis.com/v0/b/muyarantccia.appspot.com/o/homeImg%2Faroma-basil-bio-chili-162809.jpg?alt=media&token=1eee3c55-abec-4b30-a29f-294a08251355"} className="img-thumbnail"/>
                    </Link>
                    <p className = "realzar">
                        Encontrareis los productos que he ido utilizando para hacer cremas, jabones...
                    </p>
                 
                </article>
            </div> 
            {/* <pre> { JSON.stringify(this.state, undefined,2)}</pre> */}
        </section> 
        </div>      
        )
    }
}
