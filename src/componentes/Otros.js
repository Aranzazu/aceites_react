import React, { Component } from 'react';
import CallMongo from './CallMongo';
import { ShowListado } from './Template';



export default class Otros extends Component {
    state = { otros:[] }

    componentDidMount(){

        CallMongo.listaAceitesOtros().then( o =>{ console.log(o)
          this.setState({ otros: o })})       
        }
    
    render() {
        return (
            
                 <section className = "container">

                    <h1 className = "text-center titulo"> Otros Productos </h1>
                    <p>Vamos a ir viensdo que otros materiales y productos vamos a necesitar para poteciar los efectos de nuestras creaciones.
                    </p>
                    <p>Demomento aquí te muestro los que yo he utlizado hasta el momento y su valoración.</p>
                    <div className="row">{this.state.otros.map( (o,i) => {
                    return(

                        <ShowListado  key ={i} {...o} />
                    )
                
                    })}</div>
                </section>
                
                
                
       
        )
    }
}
