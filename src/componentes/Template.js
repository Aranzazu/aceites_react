import React, { Component } from 'react'
import { nbsp } from '..';
import { Link,  Redirect } from 'react-router-dom';
import CallMongo from './CallMongo';
import Carousel from 'react-bootstrap/Carousel' ;


export class BootstrapCarousel extends Component {  
    render() {  

            return (  
                <Carousel>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src="https://firebasestorage.googleapis.com/v0/b/muyarantccia.appspot.com/o/homeImg%2Fportdagrande.jpg?alt=media&token=e0ce64db-a428-4048-aab8-c5d7c75e292e"
                        alt="portada"
                        className="fotoArticulo"
                        />
                        <Carousel.Caption>
                        <h3 className="letraCarrusel">Lo natural es lo autentico</h3>
                        <p className="letraCarrusel"> Aprende a hacer cremas, jabones y acuidarte de forma natural y casera</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src="https://firebasestorage.googleapis.com/v0/b/muyarantccia.appspot.com/o/homeImg%2Falternative-aroma-aromatherapy-aromatic-275755.jpg?alt=media&token=19bdf2c3-9f78-4fa3-a70e-4ec71e68f72c"
                        alt="aceites esenciales"
                        className="fotoArticulo"
                        />

                        <Carousel.Caption>
                        <h3 >Aceites esenciales</h3>
                        <p >¿ Conoces todo lo que pueden hacer por ti ?</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src="https://firebasestorage.googleapis.com/v0/b/muyarantccia.appspot.com/o/homeImg%2Falmonds-apricot-kernel-oil-apricots-bad-432579.jpg?alt=media&token=f89b8c33-5d63-4456-925c-b4cb4e43ff35"
                        alt="aceites vegetales"
                        className="fotoArticulo"
                        />

                        <Carousel.Caption>
                        <h3 className="letraCarrusel">Aceites Vegetales</h3>
                        <p className="letraCarrusel">Lo mas adecuado para cada piel y cada receta</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src="https://firebasestorage.googleapis.com/v0/b/muyarantccia.appspot.com/o/homeImg%2Fpiqsels.com-id-oasnl.jpg?alt=media&token=f3adc6cd-0593-4c9c-92a1-1f9a3b34a808"
                        alt="Otros: extractos, hidrolatos y mas."
                        className="fotoArticulo"
                        />
                        <Carousel.Caption>
                        <h3 >¿ Que mas necesitamos ?</h3>
                        <p > Si quieres sacarles el maximo provello lo necesitas</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            )  
    }  
}   


export class ImgHome extends Component {

    render(){
        return(
            <div className="container-fluid fullB">    
            <img src= "https://firebasestorage.googleapis.com/v0/b/muyarantccia.appspot.com/o/homeImg%2Fportdagrande.jpg?alt=media&token=e0ce64db-a428-4048-aab8-c5d7c75e292e" className="img-fluid fullB" alt="portada"/>
            </div>
        )
    }
}


export class ShowProduct extends Component {
    
    state = { hidden: true}    
  
    render() {
        return (
            <div>
                <section  >
                <img src={this.props.imagen} alt={this.props.nombre} className = "fotoPr" hidden = {!this.props.imagen} />
                    <header>                    
                        <h1 className = "titulo text-center"> {this.props.nombre} </h1>
                        <p hidden = {!this.props.combina} ><spam className="subtitulo">Combina: </spam>{this.props.combina}</p>
                        <p ><spam className="subtitulo">Nota: </spam>{this.props.nota} </p>  
                    </header>
                    <p hidden = {!this.props.usos}><spam className="subtitulo">Usos: </spam>{this.props.usos}</p> 
                    <p hidden = {!this.props.indicaciones ? this.props.indicaciones: !this.props.indicaciones} > 
                        <spam className="subtitulo"> Indicaciones: </spam>
                        {this.props.indicaciones}
                    </p>
                    <p hidden = {!this.props.contraindicaciones}>
                         <spam className="subtitulo">contraindicaciones:</spam>
                          {this.props.contraindicaciones} 
                    </p>                    
                </section>
                <aside>
                    <spam className="subtitulo">Proveedores:</spam> {this.props.proveedores.map( ( p,i )=> {
                    return(
                        <p key={i} hidden = {p.precio <= 0 }>
                            
                            <a href = {p.url} target = "_blank" rel="noopener noreferrer" > {p.nombre}: </a>
                                {p.medida}: {p.precio}€ 
                        </p>
                    )
                } )} 
            </aside>
        </div>
            
        )
    }
}

export class ShowListado extends Component {
    
    state = { hidden: true, imagen:'' }

    //window.location.href    
    
    /* no se puede hacer por que en uan singelpage no se puede abrir una nueva ventana de momento
     renderLink = () =>  {
         return(
             <div>
            {window.location.href.split('/')[3] === 'show'  ?  <h2 className = "tituloF"> {this.props.nombre} </h2> : <h2 className = "text-center"> <Link to={`/show/${this.props._id}`} className = "tituloF">{this.props.nombre}</Link></h2> }
            </div>
         )
     }    */  
    renderImagen = () => {
        
        return(
            <span>
                {this.props.imagen != this.state.imagen ? <img src={this.props.imagen} alt={this.props.nombre} className = " fotoLi"/>:
    <img src="https://firebasestorage.googleapis.com/v0/b/muyarantccia.appspot.com/o/homeImg%2Fcosmetic-oil-1533662273oZu.jpg?alt=media&token=60e70ba6-f6a0-4e52-9e7e-f67e421caa78" alt={this.props.nombre} className = " fotoLi" />}  
            </span>
            
        )
    }

    render() {
        return (
                <article className="col-sm-6 col-md-4">                
                     <h2 className = "text-center"> <Link to={`/show/${this.props._id}`} className = "tituloF">{this.props.nombre}</Link></h2> 
                    <Link to={`/show/${this.props._id}` }>
                        {this.renderImagen()}                   
                   </Link> 
                    <p>Nota: {this.props.nota} </p>                                   
                </article>        
        )
    }
}

export class SelectBuscar extends Component {

    state = { campo:'', texto:'', collAceites:[], collFormulas:[], direct:false }
        
   
    render () {
         
        return(
            
            <div className="form-inline">
                <select className = "form-control" onChange = {e =>  {this.setState({campo: e.target.value})}}>
                 <option  value = "select" > Busca por </option> 
                 <option  value = "nombre" > Nombre </option>   
                 <option  value = "usos" > Usos </option>
                 <option  value = "indicaciones" > Indicaciones </option>
                 <option  value = "contraindicaciones" > Contraindicaciones </option>
                 <option  value = "nota" > Nota </option>
                </select> 

                <input className = "form-control" placeholder="Pon el nombre"
                value = { this.state.texto }
                onChange = { e => { this.setState({
                    texto: e.target.value
                })}}
                onKeyPress = { e => {
                    if ( e.key === 'Enter') { CallMongo.buscadorA( this.state.campo, this.state.texto )
                    .then ( t => {
                         this.setState({ collAceites: t, direct: true, texto:this.state.texto })}                           
                    );
                    CallMongo.buscadorF(this.state.campo, this.state.texto)
                    .then ( f => {
                        this.setState({ collFormulas: f, direct: true, texto:this.state.texto })}                           
                   );
                    }
                }}/>
              
 
            { this.state.direct ? <Redirect to = {{ pathname: '/showL', state: {collAceites: this.state.collAceites, collFormulas: this.state.collFormulas, texto:this.state.texto }}} /> : <span/> }
            </div>
        )
    }  
}



