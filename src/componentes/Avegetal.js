import React, { Component } from 'react'
import CallMongo from './CallMongo'
import { ShowListado } from './Template';
import { br } from '..';



export default class Avegetal extends Component {
    state = { vegetales: []}

    componentDidMount(){

        CallMongo.listaAceitesVegetales().then( v =>{ console.log(v)
          this.setState({ vegetales:  v })})       
        }

    render() {
        return (
            
                <section className = "container">

                    <h1 className = "text-center titulo"> Aceites Vegetales </h1>
                    
                        <p>
                            Los aceites vegetales son sustancias extraídas de los frutos oleaginosos de algunas plantas. Son ricos en vitaminas, minerales y ácidos grasos poliinsaturados que suponen un extraordinario aporte de nutrientes en la piel. Se utilizan, no solo por su aporte nutricional en la dieta sino también, por sus propiedades para mantener la piel en buen estado y prevenir su envejecimiento prematuro gracias a sus cualidades excepcionales para hidratar, nutrir y regenerar la piel.
                        </p>    
                        <p>
                            Las ventajas de utilizar los aceites vegetales como productos de hidratación y nutrición son muchas, y te aseguramos que no hace falta mucho tiempo para observar resultados evidentes. Se trata de sustancias ricas en nutrientes imprescindibles para la piel y la regeneración celular.
                        </p>
                        <p>
                            Los aceites vegetales se pueden usar solos (por ejemplo, argán para pieles maduras, jojoba para pieles grasas, caléndula para pieles sensibles, o rosa mosqueta para pieles acnéicas) aunque lo ideal es combinarlos con otros aceites vegetales indicados en cosmética natural y con aceites esenciales para utilizar sus propiedades terapéuticas y reforzar así el tratamiento que queramos llevar a cabo dependiendo de nuestro tipo de piel. Si quieres saber las proporciones para elaborar una mezcla visita nuestro artículo Cómo elaborar mezclas cosméticas con aceites vegetales y esenciales.
                        </p>  
                        <div className="row">{this.state.vegetales.map( (v,i) =>
                
                        <ShowListado  key ={i} {...v} />)}</div> 
                                                                    
                        {/* <pre> { JSON.stringify(this.state, undefined,2)}</pre> */}              

                </section> 

        )
    }
}
