import {ObjectID} from 'bson';
import { mongo } from '../index';


// todos los aggregate de todas las colleciones
export default class CallMongo {
    
    // colección aceites

    static collectionAceites(){
        
        return mongo.db('test').collection('aceites').aggregate([{$sort: {nombre: 1}}]).toArray()
    }

    static listaAceitesEsenciales(){

        return mongo.db('test').collection('aceites').aggregate([
  
        {$match:{ categoria: "Aceite Esencial"}},
        {$sort: {nombre: 1}}

        ]).toArray()
    }

    static listaAceitesVegetales(){

        return mongo.db('test').collection('aceites').aggregate([
  
        {$match:{ categoria: "Aceite Vegetal" }},
        {$sort: {nombre: 1}}

        ]).toArray()
    }

    static listaAceitesOtros(){
        return mongo.db('test').collection('aceites').aggregate([
  
            {$match:{ categoria: "Otros" }},
            {$sort: {nombre: 1}}

        ]).toArray()
    }

    static getOne(id){
        return mongo.db('test').collection('aceites').findOne({_id: new ObjectID (id)})
    }

    static buscadorA( campo, texto ){
        return mongo.db('test').collection('aceites').aggregate([
  
            {$match:{ [campo]:{$regex: texto, $options: 'i'} }},
            {$sort: {nombre: 1}}
            
        ]).toArray()
    }

     // colección formula
     static collectionFormulas(){
        
        return mongo.db('test').collection('formulasNew').aggregate([{$sort: {nombre: 1}}]).toArray()
    }
   
    

    static getOneF(id){
        return mongo.db('test').collection('formulasNew').findOne({_id: new ObjectID (id)})
    }

    static buscadorF( campo, texto ){
        return mongo.db('test').collection('formulasNew').aggregate([
  
            {$match:{ [campo]:{$regex: texto, $options: 'i'} }},
            {$sort: {nombre: 1}}
            
        ]).toArray()
    }

    
    
    
}  
