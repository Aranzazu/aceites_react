import React, { Component } from 'react'
import { ShowListado } from './Template'

export default class ShowList extends Component {


    render() {
        return (
        
        <section className = "container">
            <p className = "text-center tituloF"> El resultado de su busqueda:  </p>
            <h1 className = "text-center titulo" >{this.props.location.state.texto}</h1> 
            <div className="row">{this.props.location.state.collFormulas.map( (f,i) => {
            return <ShowListado key={i} {...f} />
            })}</div>
            <div className="row">{this.props.location.state.collAceites.map( (c,i) => {
            return <ShowListado key={i} {...c} />
            })}</div>      
        </section>
                
 
        )
    }
}
