import React,{ Component } from 'react'
import { ShowProduct } from './Template'
import CallMongo from './CallMongo'
import EsquemaClases from './EsquemaClases'




export class ShowUnProducto extends Component {
    
    state = { producto: new EsquemaClases() }

    componentDidMount(){
       
        CallMongo.getOne(this.props.match.params.id).then( p => {this.setState({producto: Object.assign(new EsquemaClases(), p) }) })     
    }


    render() {
        return (
            <div>
            <ShowProduct {...this.state.producto}/>
{/*             <pre> { JSON.stringify(this.state, undefined,2)}</pre>
 */}       </div>
        )
    }
}



