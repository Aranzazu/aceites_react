import React, { Component } from 'react';
import CallMongo from './CallMongo';
import { ShowListado } from './Template';



export default class Aesencial extends Component {
    state = { esenciales: [] }

    componentDidMount(){

        CallMongo.listaAceitesEsenciales().then( e =>{ console.log(e)
          this.setState({ esenciales:  e })})       
        }
    

    render() {
        return (

                <section className = "container">

                    <h1 className = "text-center titulo"> Aceites Esenciales </h1>
                        <p>
                            Son concentrados de materia prima vegetal, intensamente aromáticos, no grasos, volátiles y ligeros, obtenidos directamente de plantas, raíces, flores, hojas, árboles… Es un compuesto químico natural que podemos utilizar como remedio casero en numerosas situaciones.
                        </p>
                        <p>
                            Además, en la aromaterapia, los aceites esenciales son usados con fines terapéuticos.
                        </p> 
                        <div className="row">{this.state.esenciales.map( (e,i) => {
                        return(
                            <ShowListado  key ={i} {...e}  />
                        )
                    
                    })}</div> 
                        {/* <pre> { JSON.stringify(this.state, undefined,2)}</pre> */}                                       
                </section>

        )
    }
}
