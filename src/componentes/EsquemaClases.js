
export default class EsquemaClases {
    constructor(){
        this.nombre = '';
        this.precio = 0;
        this.unidadmedida = '';
        this.nota = '';
        this.usos = '';
        this.indicaciones = '';
        this.contraindicaciones = '';
        this.combina = '';
        this.provActual = '';
        this.estado = '';
        this.imagen = '';
        this.proveedores = [];
        this.autor = '';
        this.categoria = '';
    }
}

// preguntarle a domingo si puede desde la consola de mongo poner este formato de clase

export class EsquemaFormula {
    constructor(){
        this.nombre = ''; // = id
        this.precio = 0;
        this.unidadmedida = '';
        this.nota = '';
        this.usos = '';
        this.indicaciones = '';
        this.contraindicaciones = '';
        this.productos = [];
        this.posologia = '';
        this.elaboracion = '';
        this.autor = '';
        this.categoria = ''; // = tipo
    }
}




